package be.kdg.restcallapp;

import android.os.AsyncTask;
import android.util.Log;

import java.net.MalformedURLException;
import java.net.URL;

import be.kdg.restcallapp.model.Contact;

public class ContactLoader extends AsyncTask<String, Void, Contact[]> {

    @Override
    protected void onPreExecute() {
        //loopt in UI-thread
        Log.d("TEST", "onPreExe..");
        super.onPreExecute();
    }

    @Override
    protected Contact[] doInBackground(String... strings) {
        //loopt op aparte thread: REST-call doen!
        Log.d("TEST", "in the background..");
        try {
            URL url = new URL("http://jsonplaceholder.typicode.com/users");


        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return new Contact[0];
    }

    @Override
    protected void onPostExecute(Contact[] contacts) {
        //loopt in UI-thread
        Log.d("TEST", "onPostExe..");
        super.onPostExecute(contacts);
    }
}
