package be.kdg.restcallapp;

import android.app.Activity;
import android.app.ListActivity;
import android.os.Bundle;
import android.util.Log;

import java.util.concurrent.ExecutionException;

import be.kdg.restcallapp.model.Contact;

public class MainActivity extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ContactLoader loader = new ContactLoader();
        loader.execute();

        try{
            Contact[] contacts = loader.get();
            Log.d("TEST", "Aantal contacten" + contacts.length);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}
