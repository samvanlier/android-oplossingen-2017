package be.kdg.goodintentions;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends Activity {

    private Button btnBerken;
    private EditText etAfstand, etPeriode;

    private int afstand, periode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialiseViews();

        addEventHandlers();

    }

    private void addEventHandlers() {
        btnBerken.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                afstand = Integer.parseInt(etAfstand.getText().toString());
                periode = Integer.parseInt(etPeriode.getText().toString());

                //sendData(afstand, periode);
            }
        });
    }

    private void sendData(int afstand, int periode) {
        //Intent intent = new Intent(Intent.ACTION_SEND)
    }

    private void initialiseViews() {
        btnBerken = (Button) findViewById(R.id.btnBereken);
        etAfstand = (EditText) findViewById(R.id.etAfstand);
        etPeriode = (EditText) findViewById(R.id.etPeriode);
    }
}
