package be.kdg.todo.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.List;

import be.kdg.todo.model.ToDoItem;
import be.kdg.todo.model.ToDoList;

public class ToDoDao {
    private final SQLiteDatabase database;
    private final ToDoDbHelper dbHelper;

    public ToDoDao(Context context) {
        dbHelper = new ToDoDbHelper(context);
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public ToDoItem insertToDoItem(String description) {
        ContentValues values = new ContentValues();
        values.put(ToDoContract.ToDoEntry.COLUMN_NAME_DESC, description);
        long id = database.insert(ToDoContract.ToDoEntry.TABLE_NAME, null, values);
        return new ToDoItem(id, description);
    }

    public void deleteToDoItem(ToDoItem item) {
        String where = ToDoContract.ToDoEntry._ID + " = ?";
        String[] arguments = {item.getId() + ""};
        database.delete(ToDoContract.ToDoEntry.TABLE_NAME, where, arguments);
    }

    public ToDoList getAllToDoItems() {
        ToDoList list = new ToDoList();

        Cursor cursor = database.query(false, ToDoContract.ToDoEntry.TABLE_NAME,
                ToDoContract.ToDoEntry.ALL_COLUMNS, null, null, null, null, null, null, null);

        while (cursor.moveToNext()){
            long id = cursor.getLong(
                    cursor.getColumnIndex(ToDoContract.ToDoEntry._ID)
            );
            String desc = cursor.getString(
                    cursor.getColumnIndex(ToDoContract.ToDoEntry.COLUMN_NAME_DESC)
            );
            list.addItem(new ToDoItem(id, desc));
        }

        return list;
    }
}
