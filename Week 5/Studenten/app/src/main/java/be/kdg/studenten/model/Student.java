package be.kdg.studenten.model;

/**
 * Created by sam on 7/03/2017.
 */

public class Student {
    private int studentNr;
    private String achternaam;
    private String voornaam;
    private String klas;

    public Student(int studentNr, String achternaam, String voornaam, String klas) {
        this.studentNr = studentNr;
        this.achternaam = achternaam;
        this.voornaam = voornaam;
        this.klas = klas;
    }

    public int getStudentNr() {
        return studentNr;
    }

    public String getAchternaam() {
        return achternaam;
    }

    public String getVoornaam() {
        return voornaam;
    }

    public String getKlas() {
        return klas;
    }
}
