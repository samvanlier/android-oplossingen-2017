package be.kdg.studenten.model;

import android.content.Context;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sam on 7/03/2017.
 */

public class StudentManager {
    private List<Student> students;
    private int counter = 0;

    private Context context;

    public StudentManager(Context context) {
        this.students = new ArrayList<>();
        this.context = context;

        laadStudenten();
    }

    private void laadStudenten() {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(
                context.getAssets().open("studenten.txt")
        ))) {

            String line = reader.readLine();

            while (line != null) {
                String[] fields = line.split(";");
                int nr = Integer.parseInt(fields[2]);
                String voornaam = fields[1];
                String achtnaam = fields[0];
                String klas = fields[3];

                Student student = new Student(nr, achtnaam, voornaam, klas);

                students.add(student);
                line = reader.readLine();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Student next() {
        Student student;
        if (counter != students.size()){
            student = students.get(counter);

        } else {
            counter = 0;
            student = students.get(counter);
        }

        counter++;
        return student;
    }
}
