package be.kdg.studenten;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import be.kdg.studenten.model.Student;
import be.kdg.studenten.model.StudentManager;

public class MainActivity extends Activity {

    private EditText etAchternaam;
    private EditText etVoornaam;
    private EditText etStudentnr;
    private EditText etKlas;

    private Button btnNext;

    private StudentManager mgr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mgr = new StudentManager(this);

        initialiseViews();
        addEventHandlers();
        updateViews();
    }

    private void initialiseViews() {
        etAchternaam = (EditText) findViewById(R.id.et_achternaam);
        etVoornaam = (EditText) findViewById(R.id.et_voornaam);
        etStudentnr = (EditText) findViewById(R.id.et_studentnr);
        etKlas = (EditText) findViewById(R.id.et_klas);

        btnNext = (Button) findViewById(R.id.btn_next);
    }

    private void addEventHandlers() {
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateViews();
            }
        });
    }

    private void updateViews(){
        Student student = mgr.next();

        etAchternaam.setText(student.getAchternaam());
        etVoornaam.setText(student.getVoornaam());
        etKlas.setText(student.getKlas());
        etStudentnr.setText(student.getStudentNr() + "");
    }
}
