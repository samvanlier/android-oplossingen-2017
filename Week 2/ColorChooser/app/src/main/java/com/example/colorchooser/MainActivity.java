package com.example.colorchooser;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

public class MainActivity extends Activity {

    private SeekBar sbRed, sbGreen, sbBlue;
    private View rect;
    private EditText txtRed, txtGreen, txtBlue, txtHex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sbRed = (SeekBar) findViewById(R.id.sbRed);
        sbGreen = (SeekBar) findViewById(R.id.sbGreen);
        sbBlue = (SeekBar) findViewById(R.id.sbBlue);

        sbRed.setMax(255);
        sbBlue.setMax(255);
        sbGreen.setMax(255);

        rect = findViewById(R.id.kleurVak);

        txtRed = (EditText) findViewById(R.id.textRed);
        txtGreen = (EditText) findViewById(R.id.textGreen);
        txtBlue = (EditText) findViewById(R.id.textBlue);

        txtHex = (EditText) findViewById(R.id.textHex);

        addEvenHandlers();

        //updateViewFromBar();
        readSharedPrefs();
    }

    private void addEvenHandlers() {

        sbRed.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                updateViewFromBar();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        sbGreen.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                updateViewFromBar();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        sbBlue.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                updateViewFromBar();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        txtRed.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                return updateViewFromText();
            }
        });

        txtGreen.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                return updateViewFromText();
            }
        });

        txtBlue.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                return updateViewFromText();
            }
        });
    }

    private void updateViewFromBar() {
        int red, green, blue;

        red = sbRed.getProgress();
        txtRed.setText((red + ""), TextView.BufferType.EDITABLE);

        green = sbGreen.getProgress();
        txtGreen.setText((green + ""), TextView.BufferType.EDITABLE);

        blue = sbBlue.getProgress();
        txtBlue.setText((blue + ""), TextView.BufferType.EDITABLE);

        changeColor(red, green, blue);
    }

    private boolean updateViewFromText() {
        int red, green, blue;

        red = Integer.parseInt(txtRed.getText().toString());
        sbRed.setProgress(red);

        green = Integer.parseInt(txtGreen.getText().toString());
        sbGreen.setProgress(green);

        blue = Integer.parseInt(txtBlue.getText().toString());
        sbBlue.setProgress(blue);

        changeColor(red, green, blue);
        return true;
    }

    private void changeColor(int red, int green, int blue) {
        int color = Color.argb(255, red, green, blue);
        String hexColor = String.format("#%06X", (0xFFFFFF & color));
        txtHex.setText(hexColor, TextView.BufferType.NORMAL);
        txtHex.setEnabled(false);

        rect.setBackgroundColor(color);

        writeSharedPrefs();//save values!!!
    }

    private void readSharedPrefs() {
        SharedPreferences sp = getPreferences(MODE_PRIVATE);
        int defaultRed = getResources().getInteger(R.integer.default_red);
        int red = sp.getInt(getString(R.string.red_value_key),defaultRed);
        int green = sp.getInt("GREEN", R.integer.default_green);
        int blue = sp.getInt("BLUE", R.integer.default_blue);

        changeColor(red, green, blue);
    }

    private void writeSharedPrefs(){
        SharedPreferences.Editor editor = getPreferences(MODE_PRIVATE).edit();

        editor.putInt("RED_KEY", sbRed.getProgress());
        editor.putInt("GREEN_KEY", sbRed.getProgress());
        editor.putInt("BLUE_KEY", sbBlue.getProgress());

        editor.apply(); //save settings!
    }
}
