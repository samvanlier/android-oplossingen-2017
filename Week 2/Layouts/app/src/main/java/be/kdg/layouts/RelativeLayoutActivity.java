package be.kdg.layouts;

import android.app.Activity;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

public class RelativeLayoutActivity extends Activity {

    private Spinner spinner;
    private LinearLayout llSpinner;
    private ToggleButton toggleButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relative_layout);
        toggleButton = (ToggleButton) findViewById(R.id.RLTB);
        llSpinner = (LinearLayout) findViewById(R.id.LLSpinner);

        vulSpinner();
        checkTB();
        toggleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkTB();
            }
        });
    }

    public void close(View view) {
        finish();
    }

    private void vulSpinner(){
        spinner = (Spinner) findViewById(R.id.afstudeerrichtingRL);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.TI,
                android.R.layout.simple_spinner_item); //voegt de array van strings toe aan de spinner

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter); //voeg adapter toe
    }

    private void checkTB(){
        if (!toggleButton.isChecked()){
            llSpinner.setVisibility(View.GONE);
        } else {
            llSpinner.setVisibility(View.VISIBLE);
        }
    }
}
