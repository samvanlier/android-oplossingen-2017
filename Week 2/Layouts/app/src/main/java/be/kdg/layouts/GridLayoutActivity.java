package be.kdg.layouts;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class GridLayoutActivity extends Activity {

    private Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_layout);

        vulSpinner();
    }

    public void close(View view) {
        finish();
    }

    private void vulSpinner(){
        spinner = (Spinner) findViewById(R.id.afstudeerrichtingGL);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.TI,
                android.R.layout.simple_spinner_item); //voegt de array van strings toe aan de spinner

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter); //voeg adapter toe
    }
}
