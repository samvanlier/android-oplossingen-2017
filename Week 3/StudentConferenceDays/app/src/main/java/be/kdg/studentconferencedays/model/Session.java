package be.kdg.studentconferencedays.model;

import be.kdg.studentconferencedays.R;

/**
 * Created by sam on 26/02/2017.
 */

public class Session {
    public static Session[] sessions = {
        new Session("Concurrent programming in Akka.Net", "16 feb 2017", R.drawable.session1)
    };

    private String titel;
    private String datum;
    private int imageResource;

    public Session(String titel, String datum, int imageResource) {
        this.titel = titel;
        this.datum = datum;
        this.imageResource = imageResource;
    }

    public String getTitel() {
        return titel;
    }

    public String getDatum() {
        return datum;
    }

    public int getImageResource() {
        return imageResource;
    }
}
