package be.kdg.studentconferencedays.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import be.kdg.studentconferencedays.R;
import be.kdg.studentconferencedays.model.Session;

/**
 * Created by sam on 26/02/2017.
 */

public class SessionAdapter extends ArrayAdapter<Session> {

    public SessionAdapter(Context context, Session[] sessions) {
        super(context, -1, sessions);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Session session = getItem(position);
        if (convertView == null){
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.session_list_item, parent, false);
        }

        ImageView ivSessionIco = (ImageView) convertView.findViewById(R.id.sessionIco);
        Drawable image = getContext().getDrawable(session.getImageResource());
        ivSessionIco.setImageDrawable(image);

        TextView tvSessionTitel = (TextView) convertView.findViewById(R.id.sessionTitel);
        tvSessionTitel.setText(session.getTitel());

        TextView tvSessionDatum = (TextView) convertView.findViewById(R.id.sessionDatum);
        tvSessionDatum.setText(session.getDatum());

        return convertView;
    }
}
