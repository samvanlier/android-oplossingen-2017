package be.kdg.studentconferencedays.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import be.kdg.studentconferencedays.R;

public class MainActivity extends Activity {

    private ListView lvMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialiseViews();
        addEventHandlers();
    }

    private void initialiseViews(){
        lvMenu = (ListView) findViewById(R.id.menu);
    }

    private void addEventHandlers(){
        lvMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int item, long id) {
                if (item == 0){
                    Intent intent = new Intent(getApplicationContext(), SessionsActivity.class);
                    startActivity(intent);
                }
            }
        });
    }
}
