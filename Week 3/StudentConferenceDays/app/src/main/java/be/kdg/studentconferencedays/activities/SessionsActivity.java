package be.kdg.studentconferencedays.activities;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

import be.kdg.studentconferencedays.R;
import be.kdg.studentconferencedays.adapter.SessionAdapter;
import be.kdg.studentconferencedays.model.Session;

/**
 * Created by sam on 26/02/2017.
 */

public class SessionsActivity extends Activity {

    private ListView lvSessions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sessions);
        initialiseViews();
    }

    private void initialiseViews() {
        lvSessions = (ListView) findViewById(R.id.lvSessions);

        SessionAdapter adapter = new SessionAdapter(this, Session.sessions);
        lvSessions.setAdapter(adapter);
    }
}
