package be.kdg.lifecycletesting;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends Activity {

    private String test = "TEST";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(test, "in onCreate()");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(test, "in onRestart()");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(test, "in onStart()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(test, "in onPause()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(test, "in onResume()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(test, "in onDestroy()");
    }
}
