package be.kdg.yoshi.stopwatch.model;

import java.io.Serializable;

/**
 * Created by vochtenh on 14/02/2016.
 */
public class StopWatch implements Serializable{
    private int hundreds;
    private boolean running;

    public StopWatch() {
        this.hundreds = 0;
        this.running = false;
    }

    public void start() {
        this.running = true;
    }

    public void stop(){
        this.running = false;
    }

    public void reset(){
        this.running = false;
        this.hundreds = 0;
    }

    public void hundredsTick(){
        if (running) {
            hundreds++;
        }
    }

    public String getTimeString(){
        int minutes = hundreds /6000;
        int seconds = (hundreds %6000)/100;
        int localHundreds = hundreds %100;
        return String.format("%d:%02d:%02d",minutes,seconds,localHundreds);
    }
}