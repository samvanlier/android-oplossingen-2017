package be.kdg.yoshi.stopwatch;

import android.content.res.Configuration;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import be.kdg.yoshi.stopwatch.model.StopWatch;

public class MainActivity extends AppCompatActivity {
    StopWatch sw;
    TextView tvtimer;
    Button btnStart;
    Button btnStop;
    Button btnPause;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            setContentView(R.layout.stopwatch_horizontal);
        }else {
            setContentView(R.layout.activity_main);
        }

        prepareTimer();
        Toast.makeText(this.getBaseContext(), "savedInstance is niet null", Toast.LENGTH_SHORT);
        initializeViews();
        addEventHandlers();
        if (savedInstanceState != null){
            sw = (StopWatch) savedInstanceState.getSerializable("stopwatch");
        }else{
            sw = new StopWatch();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable("stopwatch", sw);
    }


    private void initializeViews() {
        tvtimer = (TextView) findViewById(R.id.tvText);
        btnStart = (Button) findViewById(R.id.btnStart);
        btnPause = (Button) findViewById(R.id.Pause);
        btnStop = (Button) findViewById(R.id.btnStop);
    }

    private void addEventHandlers() {
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sw.start();
            }
        });

        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sw.stop();
                sw.reset();
            }
        } );

        btnPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sw.stop();
            }
        });
    }

    private void prepareTimer() {
        final Handler h = new Handler();
        h.postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        sw.hundredsTick();
                        tvtimer.setText(sw.getTimeString());
                        h.postDelayed(this, 10);
                    }
                }
                , 10);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sw.stop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        sw.start();
    }
}
