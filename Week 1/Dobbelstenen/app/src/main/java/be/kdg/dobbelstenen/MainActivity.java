package be.kdg.dobbelstenen;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.util.Random;

public class MainActivity extends Activity {

    private ImageView steen1, steen2;
    private Button rollen;

    private int getal1, getal2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        if (savedInstanceState == null) {
            getal1 = 1;
            getal2 = 1;
        } else {
            getal1 = savedInstanceState.getInt("getal1");
            getal2 = savedInstanceState.getInt("getal2");
        }


        initialiseViews();
        updateViews();

        rollen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Random r = new Random();
                getal1 = r.nextInt(6) + 1;
                getal2 = r.nextInt(6) + 1;

                updateViews();
            }
        });
    }

    private void initialiseViews() {
        steen1 = (ImageView) findViewById(R.id.steen1);
        steen2 = (ImageView) findViewById(R.id.steen2);
        rollen = (Button) findViewById(R.id.button);

        updateViews();
    }

    private void updateViews() {
        switch (getal1) {
            case 1:
                steen1.setImageDrawable(getDrawable(R.drawable.die1));
                break;
            case 2:
                steen1.setImageDrawable(getDrawable(R.drawable.die2));
                break;
            case 3:
                steen1.setImageDrawable(getDrawable(R.drawable.die3));
                break;
            case 4:
                steen1.setImageDrawable(getDrawable(R.drawable.die4));
                break;
            case 5:
                steen1.setImageDrawable(getDrawable(R.drawable.die5));
                break;
            case 6:
                steen1.setImageDrawable(getDrawable(R.drawable.die6));
                break;
        }

        switch (getal2) {
            case 1:
                steen2.setImageDrawable(getDrawable(R.drawable.die1));
                break;
            case 2:
                steen2.setImageDrawable(getDrawable(R.drawable.die2));
                break;
            case 3:
                steen2.setImageDrawable(getDrawable(R.drawable.die3));
                break;
            case 4:
                steen2.setImageDrawable(getDrawable(R.drawable.die4));
                break;
            case 5:
                steen2.setImageDrawable(getDrawable(R.drawable.die5));
                break;
            case 6:
                steen2.setImageDrawable(getDrawable(R.drawable.die6));
                break;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt("getal1", getal1);
        outState.putInt("getal2", getal2);

        //super.onSaveInstanceState(outState);
    }
}
