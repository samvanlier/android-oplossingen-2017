package be.kdg.scramble;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button scramble = (Button) findViewById(R.id.scramble);
        final EditText input = (EditText) findViewById(R.id.input);

        scramble.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = input.getText().toString();
                List<Character> lijst = new ArrayList();

                for (int i = 0; i < text.length(); i++) {
                    lijst.add(text.charAt(i));
                }

                Collections.shuffle(lijst);

                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < lijst.size(); i++){
                    sb.append(lijst.get(i));
                }

                input.setText(sb.toString(), TextView.BufferType.EDITABLE);
            }
        });
    }
}
