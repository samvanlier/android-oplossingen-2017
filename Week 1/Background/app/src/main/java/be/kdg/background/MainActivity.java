package be.kdg.background;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends Activity {

    private Random random;
    private TextView background;
    private RelativeLayout scherm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        background = (TextView) findViewById(R.id.background);
        scherm = (RelativeLayout) findViewById(R.id.activity_main);

        this.changeColor();

        scherm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeColor();
            }
        });
    }

    private void changeColor() {

        random = new Random();

        int color = Color.argb(255, random.nextInt(256), random.nextInt(256), random.nextInt(256));

        getWindow().getDecorView().setBackgroundColor(color);

        background.setTextColor(Color.argb(255, random.nextInt(256), random.nextInt(256), random.nextInt(256)));


    }
}
